from __future__ import unicode_literals

from django.db import models

# Create your models here.
class CaesarCode(models.Model):
	enter_msg = models.TextField(max_length=50)
	crypt_key = models.IntegerField(default=0)


	def __str__(self):
		return self.enter_msg
