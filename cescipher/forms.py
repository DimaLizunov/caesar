from djng.forms import NgModelFormMixin, NgModelForm
from models import CaesarCode

class MyCaesarForm(NgModelFormMixin, NgModelForm):
    class Meta:
         model = CaesarCode
         fields = ('enter_msg', 'crypt_key')