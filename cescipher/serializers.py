from rest_framework import serializers
from models import CaesarCode

class CaesarCodeSerializer(serializers.ModelSerializer):
	class Meta:
		model = CaesarCode
		fields = ('encrypted')
