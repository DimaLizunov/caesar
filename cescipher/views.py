from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.views.generic import TemplateView
from rest_framework import generics
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from models import CaesarCode
from serializers import CaesarCodeSerializer
from forms import MyCaesarForm
from cesar import caesar
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

#class HomeView(TemplateView):
 #   template_name = 'cescipher/index.html'

class Index(TemplateView):
	template_name = 'index.html'


class CaesarText(generics.ListCreateAPIView):
	queryset = CaesarCode.objects.all()
	model = CaesarCode
	serializer_class = CaesarCodeSerializer

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

@api_view(['POST'])
def cesar_encode(request):
	if request.method == 'POST':
		data = JSONParser().parse(request)
		message = caesar(data.get('enter_msg'), data.get('crypt_key'), data.get('type'))
		return JsonResponse({'encrypted':message})
		# serializer = CaesarCodeSerializer(data=data)
		# if serializer.is_valid():
		# 	serializer.save()
		# 	return JsonResponse(message, status=status.HTTP_201_CREATED)
		# # return JSONResponse(serializer.errors, status=400)
