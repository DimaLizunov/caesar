var myApp = angular.module('mainApp', ['ui.router', 'chart.js']);

myApp.config(function($interpolateProvider) {
    //allow django templates and angular to co-exist
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
});