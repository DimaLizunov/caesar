import string
import collections

def caesar(rotate_string, step, type):
    rotate_string = rotate_string.encode('ascii')
    upper = collections.deque(string.ascii_uppercase)
    lower = collections.deque(string.ascii_lowercase)
    if type == 1:
        step = step * -1

    upper.rotate(step)
    lower.rotate(step)

    upper = ''.join(list(upper))
    lower = ''.join(list(lower))

    return rotate_string.translate(string.maketrans(string.ascii_uppercase, upper)).translate(string.maketrans(string.ascii_lowercase, lower))
